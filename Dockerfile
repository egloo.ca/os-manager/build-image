#-------------------------------------------------------------------------
# @author     eGloo <developer@egloo.ca>
# @copyright  2018 eGloo
# @license    http://www.apache.org/licenses/LICENSE-2.0
# @link       https://www.egloo.ca
#-------------------------------------------------------------------------

FROM centos:7.5.1804

# Install basic packages needed for mock builds
RUN set -e ; \
    yum --setopt=tsflags=nodocs -y install https://archive.fedoraproject.org/pub/epel/7Server/x86_64/Packages/e/epel-release-7-11.noarch.rpm ; \
    yum --setopt=tsflags=nodocs -y install git make rpm-build which mock expect yum-utils sudo wget nosync ; \
    yum clean all ; \
    rm -rf /var/cache/yum/*

# Copy generic mock configuration file
COPY mock.cfg /etc/mock/mock.cfg
RUN ln -sf /etc/mock/mock.cfg /etc/mock/default.cfg

# Add our "builder" user to mock
RUN useradd -m -d /home/builder -r builder ; \
    usermod -a -G mock builder

# Add sudo for ISO builder
RUN echo "builder ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/builder

# Prep home directory
USER builder
WORKDIR /home/trees
WORKDIR /home/builds
WORKDIR /home/builder
RUN echo '%dist .os7' > .rpmmacros

# Finish up
CMD ["bash"]
ENTRYPOINT []
WORKDIR /home/builder
