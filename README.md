# Build image for CentOS, ClearOS, NetifyOS and others

## Description

This project is used to create Docker images for building RPMs via mock.

## Version Branches

The master branch is not used in this project.  Instead, the version branches track the current stable development:

* os7
* os8
* and beyond

## Version Tags

Build images for minor releases are also maintained:

* os7.4
* os7.5
* and beyond
